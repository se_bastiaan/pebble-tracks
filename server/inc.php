<?php

	class Config {
		public static $username = "se_bastiaan@outlook.com";
		public static $password = "DW5hq4Swz9CpwRsG4gtsREBnDU6I-Cm6n61yGTRFhQn9BBtfkPbkyQ";
	}

	class Response {
		public $success = true;
		public $action = null;
		public $data = null;
		public $error = null;

		function __toString() {
			return json_encode($this);
		}

		function setError($error) {
			$this->success = false;
			$this->error = $error;
		}
	}

	class Station {
		public $code;
		public $short_name;
		public $full_name;
		public $lat;
		public $lng;

		function __construct(SimpleXMLElement $station) {
			$this->code = (string) $station->Code;
			$this->short_name = (string) $station->Namen->Kort;
			$this->full_name = (string) $station->Namen->Lang;
			$this->lat = (float) $station->Lat;
			$this->lng = (float) $station->Lon;
		}
	}

	class Departure {
		public $number;
		public $time;
		public $delay = null;
		public $delay_text = null;
		public $destination;
		public $train_type;
		public $route;
		public $transporter;
		public $track;
		public $track_changed = false;
		public $tip = null;
		public $comments = null;

		function __construct(SimpleXMLElement $train) {
			$this->number = (string) $train->RitNummer;
			$this->time = (string) $train->VertrekTijd;
			if((string) $train->VertrekVertraging !== "") {
				$date = new DateTime();
				$date->add(new DateInterval((string) $train->VertrekVertraging));
				$this->delay = $date->format(DateTime::ISO8601);
			}
			if((string) $train->VertrekVertragingTekst !== "") $this->delay_text = (string) $train->VertrekVertragingTekst;
			$this->destination = (string) $train->EindBestemming;
			$this->train_type = (string) $train->TreinSoort;
			$this->route = (string) $train->RouteTekst;
			$this->transporter = (string) $train->Vervoerder;
			$this->track = (string) $train->VertrekSpoor;
			if((string) $train->ReisTip !== "") $this->tip = (string) $train->ReisTip;
			if(count($train->Opmerkingen->Opmerking) > 0) {
				foreach($train->Opmerkingen->Opmerking as $comment) {
					$this->comments[] = (string) $comment;
				}
			}
			$track_changed = $train->VertrekSpoor->attributes();
			$this->track_changed = ((string)$track_changed["wijziging"]) === 'true'? true: false;
		}
	}

	class Connect {
		public $username;
		public $password;

		function __construct($username, $password) {
			$this->username = $username;
			$this->password = $password;
		}

		public function get_data($url, $params = null) {
			$content = "";
			$qry_str = "";
			if($params !== null) {
				$qry_str .= "?";
				foreach($params as $k => $v) {
					$qry_str .= $k . "=" . $v;
				}
			}

			$ch = curl_init();
			// Set query data here with the URL
			curl_setopt($ch, CURLOPT_URL, $url . $qry_str);
			curl_setopt($ch, CURLOPT_USERPWD, $this->username.":".$this->password);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$content = trim(curl_exec($ch));
			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);

			if($http_status !== 200) {
				throw new Exception('Could not connect to NS API');
			}
			return $content;
		}
	}

	class NSAPI {
		/**
		 * @var Connect
		 */
		public $connect;

		function __construct($username, $password) {
			$this->connect = new Connect($username, $password);
		}

		private function get_stations_xml() {
			return $this->connect->get_data("http://webservices.ns.nl/ns-api-stations-v2");
		}

		private function get_departures_xml($station) {
			return $this->connect->get_data("http://webservices.ns.nl/ns-api-avt", array("station"=>$station));
		}

		public function get_stations() {
			$xml = $this->get_stations_xml();
			$data = simplexml_load_string($xml);
			$result = array();

			foreach($data->Station as $station) {
				$result[] = new Station($station);
			}

			return $result;
		}

		/**
		 * @param $stationCode string station code
		 * @return Departure[]
		 */
		public function get_departures($stationCode) {
			$xml = $this->get_departures_xml($stationCode);
			$data = simplexml_load_string($xml);
			$result = array();

			foreach($data->VertrekkendeTrein as $train) {
				$result[] = new Departure($train);
			}

			return $result;
		}

		/**
		 * @param $lat float
		 * @param $lng float
		 * @return Station
		 */
		public function get_nearest_station($lat, $lng) {
			$stations = $this->get_stations();
			$distances = array();
			foreach($stations as $k => $station) {
				$distances[] = acos( sin($lat) * sin($station->lat) + cos($lat) * cos($station->lat) * cos($station->lng - $lng) ) * 6371;
			}
			$key = array_keys($distances, min($distances));
			return $stations[$key[0]];
		}

		public function get_station($code) {
			$code = strtolower($code);
			$stations = $this->get_stations();
			foreach ($stations as $k => $station) {
				if(strtolower($station->code) === $code) {
					return $station;
				}
			}
			return null;
		}
	}
