<?php
	require('inc.php');
	$api = new NSAPI(Config::$username, Config::$password);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Tracks</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="./assets/reset.css" type="text/css" />
		<link rel="stylesheet" href="./assets/style.css" type="text/css" />
		<script src="./assets/jquery.js" type="text/javascript"></script>
		<script src="./assets/script.js" type="text/javascript"></script>
	</head>
	<body>
		<header><img class="logo" src="./assets/logo.png" />Tracks</header>
		<section id="settings">
			<h2>Location</h2>
			<div class="location_radio first">
				<label for="use_location">Location-based station selection<input checked="checked" id="use_location" type="radio" name="location" value="1" /></label>
			</div>
			<div class="location_radio second">
				<label for="use_station">Manual station selection<input id="use_station" type="radio" name="location" value="0" /></label>
			</div>	
		</section>
		<section id="station">
			<h2>Station</h2>
			<select name="station" id="station-select">
				<?php 
					$stations = $api->get_stations();
					foreach($stations as $station) {
				?>
					<option value="<?php echo $station->code; ?>"><?php echo $station->full_name; ?></option>
				<?php
					}
				?>
			</select>
		</section>
		<a href="#save-data" id="save-button">Save</a>
		<div id="copyright">
			Copyright &copy; <?php echo date("Y"); ?> Sébastiaan Versteeg
		</div>
	</body>
</html>