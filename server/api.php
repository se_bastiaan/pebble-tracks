<?php
	require('inc.php');
	$api = new NSAPI(Config::$username, Config::$password);
	$response = new Response();
	$action = (isset($_GET['action']) ? $_GET['action'] : '');
	$response->action = $action;

	if($action === "stations") {
		$response->data = $api->get_stations();
	} else if($action === "location" && isset($_GET["lat"]) && isset($_GET["lng"])) {
		$lat = (float) $_GET['lat'];
		$lng = (float) $_GET['lng'];
		$response->data = $api->get_nearest_station($lat, $lng);
	} else if($action === "departures" && isset($_GET["lat"]) && isset($_GET["lng"])) {
		$lat = (float) $_GET['lat'];
		$lng = (float) $_GET['lng'];
		$station = $api->get_nearest_station($lat, $lng);
		$response->data = array(
			"station" => $station,
			"departures" => array_slice($api->get_departures($station->code), 0, 10)
		);
	} else if($action === "departures" && isset($_GET["station"])) {
		$response->data = array(
			"station" => $api->get_station($_GET["station"]),
			"departures" => array_slice($api->get_departures($_GET["station"]), 0, 10)
		);
	} else {
		$response->setError("Action not found");
	}

	header("Content-Type: application/json");
	echo $response;
